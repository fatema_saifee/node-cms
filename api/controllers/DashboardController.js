/**
 * DashboardController
 *
 * @description :: Server-side logic for managing Dashboard
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	sales: function(req, res) {
		res.view('dashboard.ejs', {});
	}
};

